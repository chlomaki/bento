# Example bento-discord-eris

This is a robust example of using Bento in a [Discord](https://discordapp.com) bot.
The actual Discord connection is handled by [Eris](https://www.npmjs.com/package/eris).
This example has been structured in a way that will let you expand as your project grows. Ayana v4 uses something very similar to this example, albeit a bit more complicated and structured slightly differently.

**Please note** this example is not for the feint of heart. It is an advanced example using a lot of Bento features.

**Do not forget to set BOT_TOKEN env variable while using this example**
