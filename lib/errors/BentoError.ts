
import { GenericError } from '@ayana/errors';

export class BentoError extends GenericError {}
