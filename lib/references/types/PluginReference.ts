
import { Plugin } from '../../plugins';

export type PluginReference = string | Plugin | Function;
