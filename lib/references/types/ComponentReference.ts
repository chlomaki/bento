
import { Component } from '../../components';

export type ComponentReference = string | Component | Function;
