
export interface VariableDefinition {
	name: string;
	default?: any;
	validator?: string;
}
