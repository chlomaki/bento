
export interface BentoState {
	components: Array<string>;
	plugins: Array<string>;
	variables: Array<string>;
}
